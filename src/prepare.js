const exec = require('./execSync.js')

exec('MySQL', 'docker run --rm -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=posts -p 3406:3306 -d mysql:8.0.22')
exec('MongoDB', 'docker run --rm -p 27017:27017 -d mongo:4.4-bionic')
exec('RabbitMQ','docker run --rm -p 5672:5672 -p 15672:15672 rabbitmq:3-management')