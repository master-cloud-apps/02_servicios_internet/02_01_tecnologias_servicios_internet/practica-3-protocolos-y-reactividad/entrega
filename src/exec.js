const exec = require('./execAsync.js')

exec("weatherservice", "node app.js")
exec("toposervice", "mvn spring-boot:run")
exec("server", "node_modules/env-cmd/bin/env-cmd.js -f ./config/dev.env node server.js")
exec("planner", "mvn spring-boot:run")