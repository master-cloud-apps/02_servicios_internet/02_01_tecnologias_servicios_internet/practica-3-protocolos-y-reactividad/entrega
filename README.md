# Entrega práctica de Protocolos de Alberto Eyo

En el siguiente directorio se encuentran los ficheros necesarios para ejecutar la práctica 
de `Alberto Eyo`. Se incluyen cuatro directorios en los que se encuentran las cinco partes de la misma:

* `server`: en este proyecto Node se encuentran el `server`y el `client`.
* `planner`: proyecto Java en Spring Boot en el que se tiene el código que planifica la planta eólica.
* `toposervice`: módulo en Java que devuelve los datos topográficos de las ciudades.
* `weatherservice`: módulo en Node que devuelve el tiempo en cada ciudad. Implementado con tecnología gRPC.


## Build de los proyectos

Para realizar el build nos ayudaremos del script `install.js` Para ello se ha creado un script en el `package.json``
que se puede invocar de la siguiente forma:

* `npm run build`. Este script descargará las dependencias y hará las builds de todos los proyectos.

Después de lanzar el script tendremos que ejecutar los dockers de los que depende la aplicación. Son tres:

* MySQL: `docker run --rm -e MYSQL_ROOT_PASSWORD=****** -e MYSQL_DATABASE=protocols -p 3306:3306 -d mysql:8.0.22`
* MongoDB: `docker run --rm -p 27017:27017 -d mongo:4.4-bionic`
* RabbitMQ: `docker run --rm -p 5672:5672 -p 15672:15672 rabbitmq:3-management`

Tendremos que sincronizar la contraseña de la Base de Datos de MySQL con el fichero `server/config/dev.env`. Ejemplo:

```
DATABASE_URL=mysql://root:root@localhost:3306/protocols
AMQP_URL=amqp://localhost
```

Entonces el run de Docker para MySQL debería ser:

`docker run --rm -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=protocols -p 3306:3306 -d mysql:8.0.22`

Recordar que el servicio de Base de Datos de MongoDB por defecto es sin contraseña, y así se ha desarrollado.
La colección será la que se llame `protocols`.

## Arranque de los servidores

Una vez las dependencias y los servicios de Base de Datos y de Broker están completados, solamente quedará levantar los servicios. Para ello lanzaremos:

* `npm run exec`

Ya podrás encontrar tu aplicación favorita en la [siguiente url][1].

[1]: http://localhost:3000/